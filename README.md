# eshop
 
1. Create a project using the Spring Framework 
2. Prepare Controller and Services which will process the incoming payload
and map it to the pre-prepared model Watch

    - Incoming payload is processed and then saved into database
 
3. Correctly validate the input and return the error HTTP status code

    - Bad request is thrown 400 in case input is invalid
    - Validation is done through hibernate
    - Custom annotation to validate base64 encoded input was created**

 
4. Save the received data to any DB (just save the images to the DB) and
return 201 CREATED

    - If everything is ok, 201 is returned. If not an exception is caught via ControllerAdvice
 
5. Prepare the application to change the format of the input data simply,
communication with the e-shop can switch to XML or another format (use
request header)

    - Content negotiation done by inserting application/xml or application/json into Accept header field
    - Default communication format is JSON
