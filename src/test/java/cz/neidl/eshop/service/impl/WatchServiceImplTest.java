package cz.neidl.eshop.service.impl;

import cz.neidl.eshop.domain.Watch;
import cz.neidl.eshop.mapper.Base64MapperImpl;
import cz.neidl.eshop.mapper.WatchMapper;
import cz.neidl.eshop.mapper.WatchMapperImpl;
import cz.neidl.eshop.model.WatchRequestDto;
import cz.neidl.eshop.model.WatchResponseDto;
import cz.neidl.eshop.repository.WatchRepository;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WatchServiceImplTest {
    @Spy
    private WatchMapper watchMapper = new WatchMapperImpl( new Base64MapperImpl() );

    @Mock
    private WatchRepository watchRepository;

    @InjectMocks
    private WatchServiceImpl watchService;

    @Test
    void getAllWatches() {

        List<Watch> watches = List.of(
                new Watch()
                        .setTitle("Bad watch")
                        .setPrice(10000)
                        .setDescription("Really really bad watch")
                        .setImg(new byte[]{88, 34, 22, 121}),
                new Watch()
                        .setTitle("Good watch")
                        .setPrice(30000)
                        .setDescription("Really really good watch")
                        .setImg(new byte[]{88, 34, 22, 125, 124, 33, 22}),
                new Watch()
                        .setTitle("Normal watch")
                        .setPrice(15000)
                        .setDescription("Really really average watch")
                        .setImg(new byte[]{88, 34, 21, 88, 12, 52}));
        when(watchRepository.findAll())
                .thenReturn(watches);

        List<WatchResponseDto> result = watchService.getAllWatches();

        for (int i = 0; i < watches.size(); i++) {
            assertThat(result.get(i).getTitle()).isEqualTo(watches.get(i).getTitle());
            assertThat(result.get(i).getPrice()).isEqualTo(watches.get(i).getPrice());
            assertThat(result.get(i).getDescription()).isEqualTo(watches.get(i).getDescription());
        }
    }

    @Test
    void saveWatch() {
        WatchRequestDto watchRequestDto = new WatchRequestDto()
                .setTitle("Good watch")
                .setPrice(30000)
                .setDescription("Really really good watch")
                .setImg("/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAhACEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9c/26/wDgqX8Hf+CcHin4baX8WNcu9BPxR1GfTtKultDLa2vkeT5s9zJnEUKG4hBbkjzAcbVZl+ia+Uv+CxP/AASw8N/8Fa/2R7j4f6tf/wBg+IdJuv7Y8M62sQf+zr5Y3jAkGNzW8iuVkQEZ+Vh8yLXwD/wSK/4LTeMf2IfjHcfshftuXQ8JeLPCeLTw3411m6C2l9bAYhiubp8K0bKMw3ROGA2SEOMkA/UT9u79vv4Zf8E4vgPd/EP4pa3/AGVo0Mi29rbQIJr7VbhvuwW0OQZJMAk8gKoLMQATXoPwX+LWi/H34O+E/HfhuaW48O+NdGs9e0uWWIxPLa3UCTwsyHlSUkUkHkdK/DD9pG7i/wCDh7/g4m8O/DCxkj1r9nv9mpJbjW5I2ElnqrQyxm+wfl3C6u1trL5XOYbd5o+N1fvXoui2fhvRrTTtOtLaw0+whS2tbW2iWKG2iRQqRoigBVVQAABgAACgC1RRRQBi/Ejx7YfCv4d694n1QXZ0zw5p1xql4LW3e4nMMETSv5cSAtI+1ThVBLHAAya/nn/aS8c/HL/g7I+IumaJ8Mvgb4V+Gvwk8K3Ujw/EnxTp7y3rJGxD266isR+9vVjY2wbD7TJLtAYf0ZUUAfzpfsdeN/id/wAGlP7S914M+M/gfw94l+C/xi1KFp/iJ4ehuJborbrIsIXJAzCJpHe1ePf+8kaOSQY3fvn+zh+0v4E/a7+EGlePfht4m07xb4S1pWNpqFmW2sVO1kZGCvG6kYZHVWU9QKt/HL4AeCP2mvhze+EPiF4T8P8AjTwzqGDPpusWMd3blhnbIFcHbIuSVdcMp5Ug81yv7Gv7Dfwr/wCCfXwkm8C/CDwnD4O8L3Goy6tNZpe3N6ZrqRI0eVpbmSSViUijXlsAIoGAKAPWaKKKACiiigAooooAKKKKAP/Z"
                );

        Watch watch = watchMapper.mapToDomain(watchRequestDto);
        when(watchRepository.save(watch)).thenReturn(watch);
        WatchResponseDto result = watchService.saveWatch(watchRequestDto);

        assertThat(result).isNotNull();

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(result.getTitle()).isEqualTo(watchRequestDto.getTitle());
        softAssertions.assertThat(result.getPrice()).isEqualTo(watchRequestDto.getPrice());
        softAssertions.assertThat(result.getDescription()).isEqualTo(watchRequestDto.getDescription());
        softAssertions.assertAll();
    }

    @Test
    void getWatchByTitle() {
        Watch watch = new Watch()
                .setTitle("Good watch")
                .setPrice(30000)
                .setDescription("Really really good watch")
                .setImg(new byte[]{88, 34, 22, 121});

        when(watchRepository.findByTitle("Good watch"))
                .thenReturn(Optional.of(watch));

        WatchResponseDto result = watchService.getWatchByTitle("Good watch");

        assertThat(result).isNotNull();

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(result.getTitle()).isEqualTo(watch.getTitle());
        softAssertions.assertThat(result.getDescription()).isEqualTo(watch.getDescription());
        softAssertions.assertThat(result.getPrice()).isEqualTo(watch.getPrice());
        softAssertions.assertAll();
    }
}