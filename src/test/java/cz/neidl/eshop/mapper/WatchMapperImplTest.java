package cz.neidl.eshop.mapper;

import cz.neidl.eshop.domain.Watch;
import cz.neidl.eshop.model.WatchRequestDto;
import cz.neidl.eshop.model.WatchResponseDto;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Base64;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest(classes = {WatchMapperImpl.class, Base64MapperImpl.class})
class WatchMapperImplTest {
    @Autowired
    private WatchMapperImpl watchMapper;

    @Test
    void mapToDto() {
        Watch watch = new Watch()
                .setTitle("Nice Watch")
                .setDescription("Very nice Watch to wear on summer evening night")
                .setPrice(1000000)
                .setImg(new byte[]{77, 97, 114, 121});

        WatchResponseDto result = watchMapper.mapToDto(watch);

        assertThat(result).isNotNull();

        SoftAssertions assertions = new SoftAssertions();

        assertions.assertThat(result.getTitle()).isEqualTo(watch.getTitle());
        assertions.assertThat(result.getDescription()).isEqualTo(watch.getDescription());
        assertions.assertThat(result.getPrice()).isEqualTo(watch.getPrice());
        assertions.assertThat(result.getImg()).isEqualTo(Base64.getEncoder().encodeToString(watch.getImg()));
        assertions.assertAll();
    }

    @Test
    void mapToDomain() {
        WatchRequestDto watchRequestDto = new WatchRequestDto()
                .setTitle("Nice Watch")
                .setDescription("Very nice Watch to wear on summer evening night")
                .setPrice(1000000)
                .setImg("R0lGODlhAQABAIAAAAUEBA");

        Watch result = watchMapper.mapToDomain(watchRequestDto);

        assertThat(result).isNotNull();

        SoftAssertions assertions = new SoftAssertions();

        assertions.assertThat(result.getTitle()).isEqualTo(watchRequestDto.getTitle());
        assertions.assertThat(result.getDescription()).isEqualTo(watchRequestDto.getDescription());
        assertions.assertThat(result.getPrice()).isEqualTo(watchRequestDto.getPrice());
        assertions.assertThat(result.getImg()).isEqualTo(Base64.getDecoder().decode(watchRequestDto.getImg()));
        assertions.assertAll();
    }
}