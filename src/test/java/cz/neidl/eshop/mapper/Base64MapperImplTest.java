package cz.neidl.eshop.mapper;

import org.junit.jupiter.api.Test;

import java.util.Base64;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class Base64MapperImplTest {

    private Base64MapperImpl base64Mapper = new Base64MapperImpl();

    @Test
    void decodeImage() {
        String img = "R0lGODlhAQABAIAAAAUEBA";
        byte[] result = base64Mapper.decodeImage(img);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(Base64.getDecoder().decode(img));
    }

    @Test
    void encodeImage() {
        byte testArray[] = new byte[]{77, 97, 114, 121};
        String result = base64Mapper.encodeImage(testArray);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(Base64.getEncoder().encodeToString(testArray));
    }
}