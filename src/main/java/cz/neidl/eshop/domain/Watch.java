package cz.neidl.eshop.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
public class Watch extends AbstractEntity{
    @NotNull
    private String title;
    @Min(value = 0)
    @Max(value = Integer.MAX_VALUE)
    private int price;
    @Size(min = 20)
    private String description;
    @Lob
    private byte[] img;
}
