package cz.neidl.eshop.api;

import cz.neidl.eshop.exception.EshopException;
import cz.neidl.eshop.model.ExceptionResponseDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value = {RuntimeException.class})
    protected ResponseEntity<Object> handleItaExceptions(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        return new ResponseEntity(new ExceptionResponseDto(ex.getMessage(), status.value(), LocalDateTime.now()), headers, status);
    }

    @ExceptionHandler(value = {EshopException.class})
    protected ResponseEntity<Object> handleEshopExceptions(EshopException ex, WebRequest request) {
        return handleExceptionInternal(ex, buildResponse(ex), new HttpHeaders(), ex.getStatus(), request);
    }

    private ExceptionResponseDto buildResponse(EshopException eshopException) {
        return new ExceptionResponseDto(eshopException.getMessage(), eshopException.getStatus().value(), LocalDateTime.now());
    }
}
