package cz.neidl.eshop.api;

import cz.neidl.eshop.model.WatchRequestDto;
import cz.neidl.eshop.model.WatchResponseDto;
import cz.neidl.eshop.service.WatchService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class WatchController {
    private final WatchService watchService;

    @GetMapping("/watches")
    public List<WatchResponseDto> getAllWatches() {
        return watchService.getAllWatches();
    }

    @PostMapping("/watches")
    @ResponseStatus(value = HttpStatus.CREATED)
    public WatchResponseDto saveWatch(@Valid @RequestBody WatchRequestDto source) {
        return watchService.saveWatch(source);
    }

    @GetMapping("/watches/{title}")
    public WatchResponseDto Watch(@PathVariable String title) {
        return watchService.getWatchByTitle(title);
    }

}
