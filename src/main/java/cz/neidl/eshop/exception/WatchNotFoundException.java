package cz.neidl.eshop.exception;

import org.springframework.http.HttpStatus;

public class WatchNotFoundException extends EshopException{

    public WatchNotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
