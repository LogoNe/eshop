package cz.neidl.eshop.model;

import lombok.Data;

@Data
public class WatchResponseDto {
    private String title;
    private int price;
    private String description;
    private String img;
}
