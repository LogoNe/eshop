package cz.neidl.eshop.model;

import cz.neidl.eshop.validator.Base64Validation;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class WatchRequestDto {
    @NotNull
    private String title;
    @Min(value=0, message="must be larger or equal to 0")
    @Max(value = Integer.MAX_VALUE)
    private int price;
    @Size(min = 20)
    private String description;
    @Base64Validation
    private String img;
}
