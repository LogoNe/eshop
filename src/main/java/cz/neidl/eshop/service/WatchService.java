package cz.neidl.eshop.service;

import cz.neidl.eshop.model.WatchRequestDto;
import cz.neidl.eshop.model.WatchResponseDto;

import javax.transaction.Transactional;
import java.util.List;

public interface WatchService {
    List<WatchResponseDto> getAllWatches();

    @Transactional
    WatchResponseDto saveWatch(WatchRequestDto watchRequestDto);

    WatchResponseDto getWatchByTitle(String title);

}
