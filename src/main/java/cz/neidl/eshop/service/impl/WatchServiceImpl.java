package cz.neidl.eshop.service.impl;

import cz.neidl.eshop.domain.Watch;
import cz.neidl.eshop.exception.WatchNotFoundException;
import cz.neidl.eshop.mapper.WatchMapper;
import cz.neidl.eshop.model.WatchRequestDto;
import cz.neidl.eshop.model.WatchResponseDto;
import cz.neidl.eshop.repository.WatchRepository;
import cz.neidl.eshop.service.WatchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j

@RequiredArgsConstructor
@Service
public class WatchServiceImpl implements WatchService {
    private final WatchRepository watchRepository;
    private final WatchMapper watchMapper;

    @Override
    public List<WatchResponseDto> getAllWatches() {
        log.info("Method: getAllWatch called");
        List<WatchResponseDto> collect = watchRepository.findAll()
                .stream()
                .map(watchMapper::mapToDto)
                .collect(Collectors.toList());
        log.debug("output {} found", collect);
        return collect;
    }

    @Override
    @Transactional
    public WatchResponseDto saveWatch(WatchRequestDto watchRequestDto) {
        log.info("Method: saveWatch called");
        Watch watch = watchRepository.save(watchMapper.mapToDomain(watchRequestDto));
        WatchResponseDto watchResponseDto = watchMapper.mapToDto(watch);
        log.debug("input {}, output {} saved", watchRequestDto, watchResponseDto);
        return watchResponseDto;
    }

    @Override
    public WatchResponseDto getWatchByTitle(String title) {
        log.info("Method: getWatchByTitle called");
        Watch byTitle = watchRepository
                .findByTitle(title)
                .orElseThrow(() -> new WatchNotFoundException("Cannot find Watch"));
        return watchMapper.mapToDto(byTitle);
    }
}
