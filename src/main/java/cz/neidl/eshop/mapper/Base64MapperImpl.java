package cz.neidl.eshop.mapper;


import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class Base64MapperImpl {
    public byte[] decodeImage(String base64img) {
        return Base64.getDecoder().decode(base64img);
    }

    public String encodeImage(byte[] img) {
        return Base64.getEncoder().encodeToString(img);
    }
}
