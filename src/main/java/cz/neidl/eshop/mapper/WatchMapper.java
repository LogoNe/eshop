package cz.neidl.eshop.mapper;

import cz.neidl.eshop.domain.Watch;
import cz.neidl.eshop.model.WatchRequestDto;
import cz.neidl.eshop.model.WatchResponseDto;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(uses = {Base64MapperImpl.class}, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface WatchMapper {
    WatchResponseDto mapToDto(Watch source);
    Watch mapToDomain(WatchRequestDto source);
}
