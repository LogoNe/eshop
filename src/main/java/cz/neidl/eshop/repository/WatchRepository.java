package cz.neidl.eshop.repository;

import cz.neidl.eshop.domain.Watch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WatchRepository extends JpaRepository<Watch,Long> {
    Optional<Watch> findByTitle(String title);
}
